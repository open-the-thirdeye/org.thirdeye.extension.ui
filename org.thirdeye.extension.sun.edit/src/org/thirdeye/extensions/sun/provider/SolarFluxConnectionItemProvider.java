/**
 */
package org.thirdeye.extensions.sun.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.thirdeye.core.mesh.provider.ConnectionItemProvider;

import org.thirdeye.extensions.sun.SolarFluxConnection;
import org.thirdeye.extensions.sun.SunPackage;

/**
 * This is the item provider adapter for a {@link org.thirdeye.extensions.sun.SolarFluxConnection} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class SolarFluxConnectionItemProvider extends ConnectionItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SolarFluxConnectionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addSunLocationECIPropertyDescriptor(object);
			addSatLocationECIPropertyDescriptor(object);
			addSolarConstantPropertyDescriptor(object);
			addSolarFluxECIPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Sun Location ECI feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSunLocationECIPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SolarFluxConnection_SunLocationECI_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SolarFluxConnection_SunLocationECI_feature", "_UI_SolarFluxConnection_type"),
				 SunPackage.Literals.SOLAR_FLUX_CONNECTION__SUN_LOCATION_ECI,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Sat Location ECI feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSatLocationECIPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SolarFluxConnection_SatLocationECI_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SolarFluxConnection_SatLocationECI_feature", "_UI_SolarFluxConnection_type"),
				 SunPackage.Literals.SOLAR_FLUX_CONNECTION__SAT_LOCATION_ECI,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Solar Constant feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSolarConstantPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SolarFluxConnection_SolarConstant_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SolarFluxConnection_SolarConstant_feature", "_UI_SolarFluxConnection_type"),
				 SunPackage.Literals.SOLAR_FLUX_CONNECTION__SOLAR_CONSTANT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Solar Flux ECI feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSolarFluxECIPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SolarFluxConnection_SolarFluxECI_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SolarFluxConnection_SolarFluxECI_feature", "_UI_SolarFluxConnection_type"),
				 SunPackage.Literals.SOLAR_FLUX_CONNECTION__SOLAR_FLUX_ECI,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns SolarFluxConnection.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/SolarFluxConnection"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((SolarFluxConnection)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_SolarFluxConnection_type") :
			getString("_UI_SolarFluxConnection_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(SolarFluxConnection.class)) {
			case SunPackage.SOLAR_FLUX_CONNECTION__SUN_LOCATION_ECI:
			case SunPackage.SOLAR_FLUX_CONNECTION__SAT_LOCATION_ECI:
			case SunPackage.SOLAR_FLUX_CONNECTION__SOLAR_CONSTANT:
			case SunPackage.SOLAR_FLUX_CONNECTION__SOLAR_FLUX_ECI:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return SunEditPlugin.INSTANCE;
	}

}
