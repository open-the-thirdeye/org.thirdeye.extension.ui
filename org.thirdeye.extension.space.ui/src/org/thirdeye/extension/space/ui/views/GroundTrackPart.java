/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.space.ui.views;

import java.awt.Image;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.inject.Inject;
import javax.inject.Named;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.experimental.chart.swt.ChartComposite;
import org.jfree.ui.Align;
import org.osgi.framework.Bundle;
import org.thirdeye.core.mesh.Variable;
import org.thirdeye.core.persistence.PersistedNode;
import org.thirdeye.core.persistence.Persistence;
import org.thirdeye.core.persistence.PersistenceFactory;
import org.thirdeye.core.persistence.SimulationRun;
import org.thirdeye.core.persistence.impl.PersistencePackageImpl;

public class GroundTrackPart {

	private JFreeChart chart;
	private Composite parent;

	@Inject
	public GroundTrackPart(Composite par) {
		//System.out.println("GroundTrackView::GroundTrackView");

		parent = par;
	}

	@Inject
	public void setSelection(@Named(IServiceConstants.ACTIVE_SELECTION) @Optional Variable var) {
		//System.out.println("GroundTrackView::setSelection");

		if (var != null) {
			//System.out.println("Var:" + var.getNode().getName() + " " + var.getFeature() + " " + var.getUnit());
			SimulationRun run = (SimulationRun) var.eContainer().eContainer().eContainer().eContainer();
			
			//if (var.getUnit().equals("rad,rad,m")) {
				chart = getChart(var, run);
				new ChartComposite(parent, SWT.NONE, chart, true);
			//}
			// chart = getChart(var);
			// new ChartComposite(parent, SWT.NONE, chart, true );
		}
	}

	public JFreeChart getChart(Variable var, SimulationRun run) {
		//System.out.println("GroundTrackView::getChart");

		PersistencePackageImpl.init();
		PersistenceFactory factory = PersistenceFactory.eINSTANCE;
		Persistence persistence = factory.createPersistence();

		@SuppressWarnings("unchecked")
		EList<EObject> list = (EList<EObject>) persistence.getBackend().getStoredNodes(var, run.eResource().toString());
		List<Object> tempList = new ArrayList<Object>();
		for (EObject node : list) {
			if (node instanceof PersistedNode) {
				EStructuralFeature tempFeature = ((PersistedNode) node).getNode().eClass()
						.getEStructuralFeature(var.getFeature());
				if (tempFeature != null) {
					Object o = ((PersistedNode) node).getNode().eGet(tempFeature);
					tempList.add(o);
				}
			}
		}
		final XYSeries series = new XYSeries("Groundtrack Data");

		Object[] tempArray = tempList.toArray();
		if (tempArray[0] instanceof EList) {
			for (int i = 1; i < tempArray.length; i++) {
				@SuppressWarnings("unchecked")
				EList<Double> temp = (EList<Double>) tempArray[i];
				double x_pixel, y_pixel;
				if (temp.size() == 3) {
					x_pixel = (temp.get(1) + Math.PI) / Math.PI * 180.0f;
					y_pixel = temp.get(0) / Math.PI * 180.0f;
					//System.out.println("Lat: " + temp.get(0) + " Lon: " + temp.get(1) + " x: " + x_pixel + " y: " + y_pixel);
					series.add(x_pixel, y_pixel);
				}
			}
		}

		final XYSeriesCollection set = new XYSeriesCollection(series);

		Image background = null;
		
		String path = "icons/Equirectangular_projection_SW.jpg";
		Bundle bundle = Platform.getBundle("org.thirdeye.extension.space.ui");
		URL url = FileLocator.find(bundle, new Path(path), null);

		try {
			background = ImageIO.read(url);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		chart = ChartFactory.createXYLineChart(null, "", "", set, PlotOrientation.VERTICAL, false, false, false);

		chart.setBackgroundImage(background);
		chart.setBackgroundImageAlpha(1.0f);
		chart.setBackgroundImageAlignment(Align.FIT);

		chart.getPlot().setBackgroundAlpha(0.0f);
		chart.getPlot().setForegroundAlpha(1.0f);

		chart.getXYPlot().getDomainAxis().setRange(0, 360);
		chart.getXYPlot().getRangeAxis().setRange(-90, 90);

		chart.getXYPlot().getDomainAxis().setTickLabelsVisible(false);
		chart.getXYPlot().getRangeAxis().setTickLabelsVisible(false);
		chart.getXYPlot().getDomainAxis().setAxisLineVisible(false);
		chart.getXYPlot().getRangeAxis().setAxisLineVisible(false);
		chart.getXYPlot().getDomainAxis().setTickMarksVisible(false);
		chart.getXYPlot().getRangeAxis().setTickMarksVisible(false);

		chart.getXYPlot().setOutlineVisible(false);

		return chart;

	}
}