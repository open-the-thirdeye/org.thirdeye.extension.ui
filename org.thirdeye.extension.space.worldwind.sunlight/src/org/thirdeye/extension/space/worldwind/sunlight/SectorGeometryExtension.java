/*
Copyright (C) 2001, 2006 United States Government
as represented by the Administrator of the
National Aeronautics and Space Administration.
All Rights Reserved.
*/
package org.thirdeye.extension.space.worldwind.sunlight;

import gov.nasa.worldwind.geom.*;
import gov.nasa.worldwind.terrain.SectorGeometry;

import java.util.ArrayList;

/**
 * @author Tom Gaskins
 * @version $Id: SectorGeometry.java 13058 2010-01-27 22:58:05Z tgaskins $
 */
public interface SectorGeometryExtension extends SectorGeometry {
    // Extraction of portions of the current tessellation inside given CONVEX regions:
    // The returned "ExtractedShapeDescription" consists of (i) the interior polygons
    // (ArrayList<Vec4[]> interiorPolys) which is the set of tessellation triangles trimmed
    // to the convex region, and (ii) the boundary edges (ArrayList<BoundaryEdge> shapeOutline)
    // which is an unordered set of pairs of vertices comprising the outer boundary of the
    // extracted region. If a boundary edge is a straight line segment, then BoundaryEdge.toMidPoint
    // will be null. Otherwise it is a vector pointing from the midpoint of the two bounding
    // vertices towards the portion of the original convex extraction region. This allows the
    // client to supersample the edge at a resolution higher than that of the current tessellation.
    public class BoundaryEdge
    {
        public Vec4[] vertices;  // an element of the returned ArrayList
        public int i1, i2;    // a pair of indices into 'vertices' describing an exterior edge
        public Vec4 toMidPoint;// if the extracted edge is linear (e.g., if the caller was the

        // 'Plane[] p' variation below), this will be null; if the edge
        // is nonlinear (e.g., the elliptical cylinder variation below),
        // this vector will point from the midpoint of (i1,i2) towards
        // the center of the desired edge.
        public BoundaryEdge(Vec4[] v, int i1, int i2)
        {
            this(v, i1, i2, null);
        }

        public BoundaryEdge(Vec4[] v, int i1, int i2, Vec4 toMid)
        {
            this.vertices = v;
            this.i1 = i1;
            this.i2 = i2;
            this.toMidPoint = toMid;
        }
    }

    public static class ExtractedShapeDescription
    {
        public ArrayList<Vec4[]> interiorPolys;
        public ArrayList<BoundaryEdge> shapeOutline;

        public ExtractedShapeDescription(ArrayList<Vec4[]> ip,
            ArrayList<SectorGeometryExtension.BoundaryEdge> so)
        {
            this.interiorPolys = ip;
            this.shapeOutline = so;
        }
    }
}

