/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.space.provider;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.edit.provider.ChangeNotifier;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.IChangeNotifier;
import org.eclipse.emf.edit.provider.IDisposable;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.thirdeye.extension.space.util.SpaceAdapterFactory;

/**
 * This is the factory that is used to provide the interfaces needed to support Viewers.
 * The adapters generated by this factory convert EMF adapter notifications into calls to {@link #fireNotifyChanged fireNotifyChanged}.
 * The adapters also support Eclipse property sheets.
 * Note that most of the adapters are shared among multiple instances.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class SpaceItemProviderAdapterFactory extends SpaceAdapterFactory implements ComposeableAdapterFactory, IChangeNotifier, IDisposable {
	/**
	 * This keeps track of the root adapter factory that delegates to this adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComposedAdapterFactory parentAdapterFactory;

	/**
	 * This is used to implement {@link org.eclipse.emf.edit.provider.IChangeNotifier}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IChangeNotifier changeNotifier = new ChangeNotifier();

	/**
	 * This keeps track of all the supported types checked by {@link #isFactoryForType isFactoryForType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Collection<Object> supportedTypes = new ArrayList<Object>();

	/**
	 * This constructs an instance.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SpaceItemProviderAdapterFactory() {
		supportedTypes.add(IEditingDomainItemProvider.class);
		supportedTypes.add(IStructuredItemContentProvider.class);
		supportedTypes.add(ITreeItemContentProvider.class);
		supportedTypes.add(IItemLabelProvider.class);
		supportedTypes.add(IItemPropertySource.class);
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.thirdeye.extension.space.Time} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TimeItemProvider timeItemProvider;

	/**
	 * This creates an adapter for a {@link org.thirdeye.extension.space.Time}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createTimeAdapter() {
		if (timeItemProvider == null) {
			timeItemProvider = new TimeItemProvider(this);
		}

		return timeItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.thirdeye.extension.space.GravityField} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GravityFieldItemProvider gravityFieldItemProvider;

	/**
	 * This creates an adapter for a {@link org.thirdeye.extension.space.GravityField}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createGravityFieldAdapter() {
		if (gravityFieldItemProvider == null) {
			gravityFieldItemProvider = new GravityFieldItemProvider(this);
		}

		return gravityFieldItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.thirdeye.extension.space.MagneticField} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MagneticFieldItemProvider magneticFieldItemProvider;

	/**
	 * This creates an adapter for a {@link org.thirdeye.extension.space.MagneticField}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createMagneticFieldAdapter() {
		if (magneticFieldItemProvider == null) {
			magneticFieldItemProvider = new MagneticFieldItemProvider(this);
		}

		return magneticFieldItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.thirdeye.extension.space.PointMass} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PointMassItemProvider pointMassItemProvider;

	/**
	 * This creates an adapter for a {@link org.thirdeye.extension.space.PointMass}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createPointMassAdapter() {
		if (pointMassItemProvider == null) {
			pointMassItemProvider = new PointMassItemProvider(this);
		}

		return pointMassItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.thirdeye.extension.space.ThermalNode} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ThermalNodeItemProvider thermalNodeItemProvider;

	/**
	 * This creates an adapter for a {@link org.thirdeye.extension.space.ThermalNode}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createThermalNodeAdapter() {
		if (thermalNodeItemProvider == null) {
			thermalNodeItemProvider = new ThermalNodeItemProvider(this);
		}

		return thermalNodeItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.thirdeye.extension.space.ThermalConnection} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ThermalConnectionItemProvider thermalConnectionItemProvider;

	/**
	 * This creates an adapter for a {@link org.thirdeye.extension.space.ThermalConnection}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createThermalConnectionAdapter() {
		if (thermalConnectionItemProvider == null) {
			thermalConnectionItemProvider = new ThermalConnectionItemProvider(this);
		}

		return thermalConnectionItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.thirdeye.extension.space.ECIToECEFProvider} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ECIToECEFProviderItemProvider eciToECEFProviderItemProvider;

	/**
	 * This creates an adapter for a {@link org.thirdeye.extension.space.ECIToECEFProvider}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createECIToECEFProviderAdapter() {
		if (eciToECEFProviderItemProvider == null) {
			eciToECEFProviderItemProvider = new ECIToECEFProviderItemProvider(this);
		}

		return eciToECEFProviderItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.thirdeye.extension.space.ECEFToLatLonAltProvider} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ECEFToLatLonAltProviderItemProvider ecefToLatLonAltProviderItemProvider;

	/**
	 * This creates an adapter for a {@link org.thirdeye.extension.space.ECEFToLatLonAltProvider}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createECEFToLatLonAltProviderAdapter() {
		if (ecefToLatLonAltProviderItemProvider == null) {
			ecefToLatLonAltProviderItemProvider = new ECEFToLatLonAltProviderItemProvider(this);
		}

		return ecefToLatLonAltProviderItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.thirdeye.extension.space.ECEFToECIProvider} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ECEFToECIProviderItemProvider ecefToECIProviderItemProvider;

	/**
	 * This creates an adapter for a {@link org.thirdeye.extension.space.ECEFToECIProvider}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createECEFToECIProviderAdapter() {
		if (ecefToECIProviderItemProvider == null) {
			ecefToECIProviderItemProvider = new ECEFToECIProviderItemProvider(this);
		}

		return ecefToECIProviderItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.thirdeye.extension.space.PointMassGravity} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PointMassGravityItemProvider pointMassGravityItemProvider;

	/**
	 * This creates an adapter for a {@link org.thirdeye.extension.space.PointMassGravity}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createPointMassGravityAdapter() {
		if (pointMassGravityItemProvider == null) {
			pointMassGravityItemProvider = new PointMassGravityItemProvider(this);
		}

		return pointMassGravityItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.thirdeye.extension.space.FixedAttitude} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FixedAttitudeItemProvider fixedAttitudeItemProvider;

	/**
	 * This creates an adapter for a {@link org.thirdeye.extension.space.FixedAttitude}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createFixedAttitudeAdapter() {
		if (fixedAttitudeItemProvider == null) {
			fixedAttitudeItemProvider = new FixedAttitudeItemProvider(this);
		}

		return fixedAttitudeItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.thirdeye.extension.space.SCLocalToECIProvider} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SCLocalToECIProviderItemProvider scLocalToECIProviderItemProvider;

	/**
	 * This creates an adapter for a {@link org.thirdeye.extension.space.SCLocalToECIProvider}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createSCLocalToECIProviderAdapter() {
		if (scLocalToECIProviderItemProvider == null) {
			scLocalToECIProviderItemProvider = new SCLocalToECIProviderItemProvider(this);
		}

		return scLocalToECIProviderItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.thirdeye.extension.space.ECItoSCLocalProvider} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ECItoSCLocalProviderItemProvider ecItoSCLocalProviderItemProvider;

	/**
	 * This creates an adapter for a {@link org.thirdeye.extension.space.ECItoSCLocalProvider}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createECItoSCLocalProviderAdapter() {
		if (ecItoSCLocalProviderItemProvider == null) {
			ecItoSCLocalProviderItemProvider = new ECItoSCLocalProviderItemProvider(this);
		}

		return ecItoSCLocalProviderItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.thirdeye.extension.space.AttitudeModel} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AttitudeModelItemProvider attitudeModelItemProvider;

	/**
	 * This creates an adapter for a {@link org.thirdeye.extension.space.AttitudeModel}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createAttitudeModelAdapter() {
		if (attitudeModelItemProvider == null) {
			attitudeModelItemProvider = new AttitudeModelItemProvider(this);
		}

		return attitudeModelItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.thirdeye.extension.space.RFLink} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RFLinkItemProvider rfLinkItemProvider;

	/**
	 * This creates an adapter for a {@link org.thirdeye.extension.space.RFLink}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createRFLinkAdapter() {
		if (rfLinkItemProvider == null) {
			rfLinkItemProvider = new RFLinkItemProvider(this);
		}

		return rfLinkItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.thirdeye.extension.space.ElectricalLoad} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ElectricalLoadItemProvider electricalLoadItemProvider;

	/**
	 * This creates an adapter for a {@link org.thirdeye.extension.space.ElectricalLoad}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createElectricalLoadAdapter() {
		if (electricalLoadItemProvider == null) {
			electricalLoadItemProvider = new ElectricalLoadItemProvider(this);
		}

		return electricalLoadItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.thirdeye.extension.space.SolarCell} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SolarCellItemProvider solarCellItemProvider;

	/**
	 * This creates an adapter for a {@link org.thirdeye.extension.space.SolarCell}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createSolarCellAdapter() {
		if (solarCellItemProvider == null) {
			solarCellItemProvider = new SolarCellItemProvider(this);
		}

		return solarCellItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.thirdeye.extension.space.Receiver} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReceiverItemProvider receiverItemProvider;

	/**
	 * This creates an adapter for a {@link org.thirdeye.extension.space.Receiver}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createReceiverAdapter() {
		if (receiverItemProvider == null) {
			receiverItemProvider = new ReceiverItemProvider(this);
		}

		return receiverItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link org.thirdeye.extension.space.Transmitter} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TransmitterItemProvider transmitterItemProvider;

	/**
	 * This creates an adapter for a {@link org.thirdeye.extension.space.Transmitter}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createTransmitterAdapter() {
		if (transmitterItemProvider == null) {
			transmitterItemProvider = new TransmitterItemProvider(this);
		}

		return transmitterItemProvider;
	}

	/**
	 * This returns the root adapter factory that contains this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComposeableAdapterFactory getRootAdapterFactory() {
		return parentAdapterFactory == null ? this : parentAdapterFactory.getRootAdapterFactory();
	}

	/**
	 * This sets the composed adapter factory that contains this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParentAdapterFactory(ComposedAdapterFactory parentAdapterFactory) {
		this.parentAdapterFactory = parentAdapterFactory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object type) {
		return supportedTypes.contains(type) || super.isFactoryForType(type);
	}

	/**
	 * This implementation substitutes the factory itself as the key for the adapter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter adapt(Notifier notifier, Object type) {
		return super.adapt(notifier, this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object adapt(Object object, Object type) {
		if (isFactoryForType(type)) {
			Object adapter = super.adapt(object, type);
			if (!(type instanceof Class<?>) || (((Class<?>)type).isInstance(adapter))) {
				return adapter;
			}
		}

		return null;
	}

	/**
	 * This adds a listener.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addListener(INotifyChangedListener notifyChangedListener) {
		changeNotifier.addListener(notifyChangedListener);
	}

	/**
	 * This removes a listener.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeListener(INotifyChangedListener notifyChangedListener) {
		changeNotifier.removeListener(notifyChangedListener);
	}

	/**
	 * This delegates to {@link #changeNotifier} and to {@link #parentAdapterFactory}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void fireNotifyChanged(Notification notification) {
		changeNotifier.fireNotifyChanged(notification);

		if (parentAdapterFactory != null) {
			parentAdapterFactory.fireNotifyChanged(notification);
		}
	}

	/**
	 * This disposes all of the item providers created by this factory. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void dispose() {
		if (timeItemProvider != null) timeItemProvider.dispose();
		if (gravityFieldItemProvider != null) gravityFieldItemProvider.dispose();
		if (magneticFieldItemProvider != null) magneticFieldItemProvider.dispose();
		if (pointMassItemProvider != null) pointMassItemProvider.dispose();
		if (thermalNodeItemProvider != null) thermalNodeItemProvider.dispose();
		if (thermalConnectionItemProvider != null) thermalConnectionItemProvider.dispose();
		if (eciToECEFProviderItemProvider != null) eciToECEFProviderItemProvider.dispose();
		if (ecefToLatLonAltProviderItemProvider != null) ecefToLatLonAltProviderItemProvider.dispose();
		if (ecefToECIProviderItemProvider != null) ecefToECIProviderItemProvider.dispose();
		if (pointMassGravityItemProvider != null) pointMassGravityItemProvider.dispose();
		if (fixedAttitudeItemProvider != null) fixedAttitudeItemProvider.dispose();
		if (scLocalToECIProviderItemProvider != null) scLocalToECIProviderItemProvider.dispose();
		if (ecItoSCLocalProviderItemProvider != null) ecItoSCLocalProviderItemProvider.dispose();
		if (attitudeModelItemProvider != null) attitudeModelItemProvider.dispose();
		if (rfLinkItemProvider != null) rfLinkItemProvider.dispose();
		if (electricalLoadItemProvider != null) electricalLoadItemProvider.dispose();
		if (solarCellItemProvider != null) solarCellItemProvider.dispose();
		if (receiverItemProvider != null) receiverItemProvider.dispose();
		if (transmitterItemProvider != null) transmitterItemProvider.dispose();
	}

}
