/********************************************************************************
 * Copyright (c) 2014-2017 Claas Ziemke
 *
 * This file is part of ThirdEye.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Claas Ziemke - claas.ziemke@gmx.net - initial implementation
 ********************************************************************************/
package org.thirdeye.extension.space.worldwind.ui;

import javax.inject.Inject;
import javax.inject.Named;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.Panel;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.UISynchronize;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.thirdeye.core.mesh.Mesh;
import org.thirdeye.core.mesh.Node;
import org.thirdeye.core.persistence.SimulationRun;
import org.thirdeye.extension.space.worldwind.TimeDependentRenderable;

import gov.nasa.worldwind.Model;
import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.awt.WorldWindowGLCanvas;
import gov.nasa.worldwind.layers.LayerList;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.render.Renderable;

public class WorldWindPart {
	
	public static final String ID = WorldWindPart.class.getName();
	
	private final WorldWindowGLCanvas world = new WorldWindowGLCanvas();
	private final List<TimeDependentRenderable> objects = new ArrayList<TimeDependentRenderable>();
	private Composite parent;
	private Mesh mesh;
	
	@Inject UISynchronize sync;
	
	@Inject
	public WorldWindPart(Composite par) {
		System.out.println("WorldWindView::WorldWindView");

		parent = par;
		Model m = (Model) WorldWind.createConfigurationComponent(AVKey.MODEL_CLASS_NAME);
		world.setModel(m);
		LayerList layers = world.getModel().getLayers();

		RenderableLayer timeDependentlayer = new RenderableLayer();
		timeDependentlayer.setName("TimeDependent Layer");
		layers.add(timeDependentlayer);

		Composite top = new Composite(parent, SWT.EMBEDDED);
		top.setLayoutData(new GridData(GridData.FILL_BOTH));
		    
		Frame worldFrame = SWT_AWT.new_Frame(top);
		Panel panel = new java.awt.Panel(new java.awt.BorderLayout());
		  
		worldFrame.add(panel);
		panel.add(world, BorderLayout.CENTER);
		
		parent.setLayoutData(new GridData(GridData.FILL_BOTH));
		setTimer();
	}

	@Inject
	public void setSelection(
	 @Named(IServiceConstants.ACTIVE_SELECTION)@Optional EObject eObj ){
		//System.out.println("WorldWindView::setSelection");

		if(eObj instanceof Mesh) {
			//System.out.println("WWMesh: " + eObj.toString());
			mesh = (Mesh) eObj;
			objects.clear();
			for( Node mod: mesh.getModelNodes() ) {
				if(mod instanceof TimeDependentRenderable ) {
					if(objects.contains((TimeDependentRenderable) mod)) {
						objects.remove((TimeDependentRenderable) mod);
						System.out.println("Removed old instance of " + mod.toString() + " from render list...");
					}
					objects.add((TimeDependentRenderable) mod);
					System.out.println("Added " + mod.toString() + " to render list...");
				}
			}
			LayerList layers = world.getModel().getLayers();
			RenderableLayer timeDependentlayer = (RenderableLayer) layers.getLayerByName("TimeDependent Layer");	
			for( TimeDependentRenderable obj: objects ) {
				timeDependentlayer.addRenderable((Renderable) obj);
			}
		} else if(eObj instanceof SimulationRun) {
			//System.out.println("WWRunner: " + eObj.toString());
			mesh = ((SimulationRun)eObj).getSequence().getMesh();
			objects.clear();
			for( Node mod: mesh.getModelNodes() ) {
				if(mod instanceof TimeDependentRenderable ) {
					if(objects.contains((TimeDependentRenderable) mod)) {
						objects.remove((TimeDependentRenderable) mod);
						System.out.println("Removed old instance of " + mod.toString() + " from render list...");
					}
					objects.add((TimeDependentRenderable) mod);
					System.out.println("Added " + mod.toString() + " to render list...");
				}
			}
			LayerList layers = world.getModel().getLayers();
			RenderableLayer timeDependentlayer = (RenderableLayer) layers.getLayerByName("TimeDependent Layer");	
			for( TimeDependentRenderable obj: objects ) {
				timeDependentlayer.addRenderable((Renderable) obj);
			}
		}
	 }
   
	public void setTimer() {
		   new Thread(new Runnable() {
			      public void run() {
			         while (true) {
			            try { Thread.sleep(100); } catch (Exception e) { }
			            sync.syncExec(new Runnable() {
			               public void run() {
			            	   //System.out.println("WWRepaint!");
			            	   world.repaint();
			               }
			            });
			         }
			      }
			   }).start();
	}
}